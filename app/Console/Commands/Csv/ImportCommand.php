<?php

namespace App\Console\Commands\Csv;

use App\Models\Product;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:import {patch : Patch to CSV file} {--test : Test file without uploading in database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import CSV file';

    /**
     * @var string
     */
    private $file;

    /**
     * @var int
     */
    private $countRows;

    /**
     * @var int
     */
    private $countInsertRows;

    /**
     * @var int
     */
    private $countValidRows;

    /**
     * @var int
     */
    private $countSkippedRows;

    /**
     * @var bool
     */
    private $isTest = false;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->simpleValidateFile();
        $this->processingFile();

        $this->info('Count rows in file: ' . $this->countRows);
        $this->info('Count success rows: ' . $this->countValidRows);
        $this->info('Count insert rows: ' . $this->countInsertRows);
        $this->warn('Count skipped rows: ' . $this->countSkippedRows);
    }

    /**
     * Simple validation file if exist and type CSV
     *
     * @return bool
     */
    private function simpleValidateFile(): bool
    {
        $this->file = $this->argument('patch');
        $this->isTest = $this->option('test');

        if($this->isTest){
            $this->warn('Test mode');
        }

        $this->info('Patch to CSV file: ' . $this->file);
        if(!is_file($this->file)){
            $this->error('File does not exist');

            return false;
        }

        $info = pathinfo($this->file);
        if(!$info['extension'] === '.csv'){
            $this->error('File is not CSV');

            return false;
        }

        return true;
    }

    /**
     * Simple validation file if exist and type CSV
     *
     * @return bool
     */
    private function processingFile(): bool
    {
        $handle = fopen($this->file, 'rb');
        $header = true;

        $products = [];
        $productSkipped = [];

        while ($csvLine = fgetcsv($handle, 1000, ',')) {

            if ($header) {
                $header = false;
            } else {
                $product['code']     = $csvLine[0] ?? '';
                $product['name']     = $csvLine[1] ?? '';
                $product['desc']     = $csvLine[2] ?? '';
                $product['stock']    = $csvLine[3] ?? '';
                $product['cost']     = $csvLine[4] ?? '';
                $product['discount'] = isset($csvLine[5]) ? true : false;

                $this->countRows++;

                $skipRow = false;
                foreach ($product as $name => $value) {
                    if (empty($value)) {
                        $skipRow = true;
                        continue;
                    }
                    if ( $name === 'stock' && $value < 10 ) {
                        $skipRow = true;
                        continue;
                    }
                    if ( $name === 'cost' && ($value < 5 || $value > 1000)) {
                        $skipRow = true;
                        continue;
                    }
                }

                if ($skipRow) {
                    $this->countSkippedRows++;
                    $productSkipped[] = $product;
                    continue;
                }

                $this->countValidRows++;

                unset($product['discount']);
                $product['created_at'] = $product['discontinued_at'] = now();
                $products[] = $product;
            }

        }

        if(!$this->isTest){
            $this->countInsertRows = Product::insertOnDuplicateKey($products, ['stock', 'cost']);
        }

        $tableHeaders = ['Product Code', 'Product Name', 'Product Description', 'Stock', 'Cost in GBP', 'Discontinued'];

        $this->info('Valid rows: ' . $this->countValidRows);
        $this->table($tableHeaders, $products);

        $this->info('Skipped rows: ' . $this->countSkippedRows);
        $this->table($tableHeaders, $productSkipped);

        return true;
    }
}
