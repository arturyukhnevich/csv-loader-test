### Requirement  ###
* PHP 7.1+
* MySQL 5.7+
* Composer

### Run ###
1. `php composer install`
2. `php artisan migrate`
3. `cp .env.exapmple .env`
4. `php artisan key:generate`
5. In the file `.env` configure the database connection
6. `php artisan csv:import stock.csv` or`php artisan csv:import stock.csv --test`