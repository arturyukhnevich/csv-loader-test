<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable(false);
            $table->string('desc')->nullable(false);
            $table->string('code', 10)->nullable(false);
            $table->integer('stock')->nullable(false);
            $table->float('cost')->nullable(false);
            $table->timestamp('discontinued_at');
            $table->timestamps();

            $table->unique('code', 'products_code_unique');
        });

        DB::statement("ALTER TABLE `products` comment 'Stores product data'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
